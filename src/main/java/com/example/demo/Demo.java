package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class Demo {


    @RequestMapping("/greeting")
    public String greeting(@RequestParam(value="name", defaultValue="Ivan") String name) {
        return name + " hello";
    }
}
